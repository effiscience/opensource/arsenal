export class FrontendCachedValue<Type> {
    private _promise?: Promise<Type>;
    private _value?: Type;
    private _timeoutId?: any;

    private _handler: () => Promise<Type>;
    private _timeout: number;

    constructor(handler: () => Promise<Type>, timeout: number = 60_000)
    {
        this._handler = handler;
        this._timeout = timeout;
    }

    getValue(): Promise<Type>
    {
        if (this._value !== undefined) {
            console.log('cached');
            return Promise.resolve(this._value);
        }

        if (this._promise) {
            console.log('someone do it');
            return this._promise;
        }

        this._promise = this._handler();
        this._promise.then(r => {
            this._value = r;
            delete this._promise;
            if (this._timeout > 0) {
                this._timeoutId = setTimeout(() => {
                    delete this._value;
                }, this._timeout);
            }
        });

        return this._promise;
    }

    purge(): void
    {
        if (this._timeoutId !== undefined) {
            clearTimeout(this._timeoutId);
        }

        delete this._value;
    }
}