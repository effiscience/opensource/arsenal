import {FrontendCachedValue} from "./frontend-cached-value";

export class FrontendCachedValueByParameter<Type, Parameter = any> {
    private _cachedValuesByParams: {[k: string]: FrontendCachedValue<Type>};

    private _handler: (param: Parameter) => Promise<Type>;
    private _timeout: number;

    constructor(handler: (param: Parameter) => Promise<Type>, timeout: number = 60_000)
    {
        this._cachedValuesByParams = {};
        this._handler = handler;
        this._timeout = timeout;
    }

    getValue(param: Parameter): Promise<Type>
    {
        const key = JSON.stringify(param);
        if (this._cachedValuesByParams[key]) {
            return this._cachedValuesByParams[key].getValue();
        }

        const cachedValue = new FrontendCachedValue(() => {
            return this._handler(param);
        }, this._timeout);
        this._cachedValuesByParams[key] = cachedValue;
        return cachedValue.getValue();
    }

    purge(): void
    {
        for (const key of Object.keys(this._cachedValuesByParams)) {
          this._cachedValuesByParams[key].purge();
        }

        delete this._cachedValuesByParams;
        this._cachedValuesByParams = {};
    }
}
