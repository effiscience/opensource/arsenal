export class Arrays {
    private constructor() {}

    public static isEmpty<Type = any>(array: Type[]): boolean
    {
        return !array || 0 === array.length;
    }

    public static pop_back<Type = any>(array: Type[]): Type|undefined
    {
        if (Arrays.isEmpty(array)) {
            return undefined;
        }

        return array.pop();
    }

    public static push_back<Type = any>(array: Type[], item: Type): void
    {
        array.push(item);
    }
}