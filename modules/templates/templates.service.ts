import Handlebars from 'handlebars';
import { Lang, I18nService } from './../i18n/i18n.service';

class _TemplateService {
    private engine: typeof Handlebars;

    constructor()
    {
        this.engine = Handlebars.create();
    }

    public template(template: string, parameters: any): string
    {
        return this.engine.compile(template)(parameters);
    }

    public translate(parameters: any, key: string, lang?: Lang): string
    {
        return this.template(I18nService.translate(key, lang), parameters);
    }
}

const TemplateService = new _TemplateService();
export default TemplateService;