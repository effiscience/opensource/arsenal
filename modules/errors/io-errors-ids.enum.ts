export enum IoErrorsIDs {
    FILE_NOT_FOUND = 'FILE_NOT_FOUND',
    FILE_MOVE_FAILURE = 'FILE_MOVE_FAILURE',
}