import en from './en.json';
import fr from './fr.json';

export type Lang = 'en' | 'fr';

interface Translation {
    [lang: string]: string,
}

interface Translations {
    [key: string]: Translation,
}

class _I18nService {
    private _translations: Translations;
    private _defaultLang: Lang;

    constructor()
    {
        this._translations = {};
        this._defaultLang = 'en';
        this._load('en', en);
        this._load('fr', fr);
    }

    private _load(lang: Lang, data: {[key: string]: string}): void
    {
        const keys = Object.keys(data);
        for (let key of keys) {
            if (!this._translations[key]) {
                this._translations[key] = {};
            }

            this._translations[key][lang] = data[key];
        }
    }

    public setDefaultLang(lang: Lang): void
    {
        this._defaultLang = lang;
    }

    public translate(key: string, lang?: Lang): string
    {
        if (!lang) {
            lang = this._defaultLang;
        }

        if (!this._translations[key]) {
            return key;
        }

        if (!this._translations[key][lang]) {
            if (!this._translations[key][this._defaultLang]) {
                return key;
            }
            
            return this._translations[key][this._defaultLang];
        }

        return this._translations[key][lang];
    }
}

export const I18nService = new _I18nService();