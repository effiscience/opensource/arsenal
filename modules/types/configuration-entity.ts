import { Modules } from './modules';
import { Configuration } from './configuration';
import { ConfigurationParameters } from './configuration-parameters';

export interface ConfigurationEntity extends Configuration {
    parameters: ConfigurationParameters,
    modules: Modules,
}