import { Parameter } from './parameter';

export interface Parameters {
    [keys: string]: Parameter,
}
