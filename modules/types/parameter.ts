export interface Parameter {
    type: 'text' | 'number' | 'checkbox' | 'radio',
    translatableDisplayName?: boolean,
    displayName?: string,
    value: string,
    oneOf?: string[]
}