import { File } from "./file";

export interface FileTransfer {
    from: File,
    to: File,
}