import { Parameters } from './parameters';
import { SystemParameters } from './system-parameters';

export interface ConfigurationParameters {
    system: SystemParameters,
    user: Parameters,
}
