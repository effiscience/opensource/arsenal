import { ModuleParameters } from './module-parameters';

export interface Module {
    parameters: ModuleParameters,
}