export interface File {
    type: 'local' | 'remote',
    namespaces: string[],
    namespace?: string,
    deployments?: string[],
    deployment?: string,
    path: string,
}
