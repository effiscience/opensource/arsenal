import { Code } from './code';

export interface JsonError {
    cluster?: string,
    namespace?: string,
    deployment?: string,
    pod?: string,
    code: Code,
    what: string,
    innerError?: string|JsonError,
}