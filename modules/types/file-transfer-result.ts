import { JsonError } from './json-error';

export interface FileTransferResult {
    success: boolean,
    error?: JsonError,
}