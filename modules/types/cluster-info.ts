export interface ClusterInfo {
    id: string,
    name: string,
    kubecontext: string,
    kubeconfigPath: string,
}