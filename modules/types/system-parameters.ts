import { Lang } from './../i18n/i18n.service';
import { Parameter } from './parameter';
import { Parameters } from './parameters';

export interface SystemParameters extends Parameters {
    language: Parameter & {
        type: 'radio',
        value: Lang,
        oneOf: Lang[],
    }
}
