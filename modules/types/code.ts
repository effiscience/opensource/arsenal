export interface Code {
    origin: string,
    domain: string,
    id: string,
}