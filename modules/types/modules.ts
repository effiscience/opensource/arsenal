import { Module } from './module';

export interface Modules {
    [key: string]: Module,
}