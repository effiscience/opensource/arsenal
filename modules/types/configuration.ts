export interface Configuration {
    translatableName?: boolean,
    name: string,
    readOnlyParameters: boolean,
    parametersId: string,
    modulesId: string,
}