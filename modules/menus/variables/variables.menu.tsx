import React, { ChangeEvent, MouseEvent } from "react";
import { Renderer } from "@k8slens/extensions";
import { ArrowDropDown } from "@mui/icons-material";
import { Accordion, AccordionDetails, AccordionSummary, Box, Button, ButtonGroup, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material";
import { I18nService } from "../../i18n/i18n.service";
import { IDs } from "../../ids";
import { ParametersTable } from "../../widgets/tables/parameters.table";
import { ArsenalStore } from "../../stores/arsenal.store";
import { Parameter } from "../../types/parameter";
import { ConfigurationEntity } from "../../types/configuration-entity";

export class VariablesMenu extends React.Component<{ extension: Renderer.LensExtension, store: ArsenalStore }, { propertyName: string, propetyValue: string }> {
    configuration: ConfigurationEntity;
    
    constructor(props: any)
    {
        super(props);
        this.configuration = this.props.store.getDefaultConfiguration();
        this.addProperty = this.addProperty.bind(this);
        this.setPropertyName = this.setPropertyName.bind(this);
        this.setPropertyValue = this.setPropertyValue.bind(this);
        this.state = {
            propertyName: '',
            propetyValue: '',
        };
    }

    private setPropertyName(event: ChangeEvent<HTMLInputElement>)
    {
        this.setState({
            propertyName: event.target.value,
        })
    }

    private setPropertyValue(event: ChangeEvent<HTMLInputElement>)
    {
        this.setState({
            propetyValue: event.target.value,
        })
    }

    private addProperty(event: MouseEvent<HTMLButtonElement>)
    {
        this.configuration.parameters.user[this.state.propertyName] = {
            type: 'text',
            value: this.state.propetyValue,
            displayName: this.state.propertyName,
            translatableDisplayName: false,
        };
        this.props.store.save();
        this.setState({
            propertyName: '',
            propetyValue: '',
        })
    }

    render() {
        return (
            <Renderer.Component.TabLayout>
                <Accordion>
                    <AccordionSummary
                        expandIcon={<ArrowDropDown/>}
                        aria-controls={IDs.PLUGIN_VARIABLES_SYSTEM}
                        id={IDs.PLUGIN_VARIABLES_SYSTEM}>
                        <Typography>{I18nService.translate(IDs.PLUGIN_VARIABLES_SYSTEM)}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <ParametersTable parameters={this.configuration.parameters.system} store={this.props.store} deletable={false}/>
                    </AccordionDetails>
                </Accordion>
                <Accordion>
                    <AccordionSummary
                        expandIcon={<ArrowDropDown/>}
                        aria-controls={IDs.PLUGIN_VARIABLES_USER}
                        id={IDs.PLUGIN_VARIABLES_USER}>
                        <Typography>{I18nService.translate(IDs.PLUGIN_VARIABLES_USER)}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <ParametersTable parameters={this.configuration.parameters.user} store={this.props.store} deletable={true}/>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <TextField fullWidth={true} value={this.state.propertyName} onChange={this.setPropertyName}/>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField fullWidth={true} value={this.state.propetyValue} onChange={this.setPropertyValue}/>
                            </Grid>
                            <Grid item xs={4} padding="auto">
                                <Box display="flex" justifyContent="center">
                                    <Button onClick={this.addProperty}>Add</Button>
                                </Box>
                            </Grid>
                        </Grid>
                    </AccordionDetails>
                </Accordion>
            </Renderer.Component.TabLayout>
        )
    }
}
