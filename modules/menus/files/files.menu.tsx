import React, { CSSProperties } from "react";
import { Renderer } from "@k8slens/extensions";
import { Box, Button, ButtonGroup, Grid, Paper } from "@mui/material";

import {FileSelector} from "../../widgets/cards/file-selector.card";
import {File} from "../../types/file";
import { ArrowDownward } from "@mui/icons-material";
import { ArsenalStore } from "../../stores/arsenal.store";
import { ConfigurationEntity } from "../../types/configuration-entity";
import { Module } from "../../types/module";
import { I18nService } from "../../i18n/i18n.service";
import { IDs } from "../../ids";
import { KubernetesFrontendService } from "../../services/kubernetes/kubernetes.frontend-service";
import TemplateService from "../../templates/templates.service";
import { FileTransfer } from "../../types/file-transfer";
import { FileTransferResult } from "../../types/file-transfer-result";
import { ErrorsService } from "../../services/errors/errors.service";

type FileMenuTransferItem = FileTransfer & {result?: FileTransferResult};

export class FilesMenu extends React.Component<{ extension: Renderer.LensExtension, store: ArsenalStore }, {
    transfers: FileMenuTransferItem[]
}> {
    private configuration: ConfigurationEntity;
    private fileUploadModule: Module;
    private from: File;
    private to: File;

    constructor(props: any) {
        super(props);
        this.transfert = this.transfert.bind(this);
        this.clearTransfer = this.clearTransfer.bind(this);
        this.state = {
            transfers: [],
        };

        this.configuration = this.props.store.getConfiguration('default');
        if (!this.configuration.modules.fileUpload) {
            this.configuration.modules.fileUpload = {
                parameters: {
                    from: {
                        type: 'local',
                        namespaces: [],
                        path: '',
                    } as File,
                    to : {
                        type: 'local',
                        namespaces: [],
                        path: '',
                    } as File,
                }
            };
        }
        this.fileUploadModule = this.configuration.modules.fileUpload;
        this.from = this.fileUploadModule.parameters.from;
        this.to = this.fileUploadModule.parameters.to;
    }

    private transfert()
    {
        this.props.store.save();
        const parameters = ArsenalStore.GetParameters(this.configuration); 
        const transfer: FileMenuTransferItem = {
            from: Object.assign({}, this.from, {
                path: TemplateService.template(this.from.path, parameters),
            }),
            to: Object.assign({}, this.to, {
                path: TemplateService.template(this.to.path, parameters),
            })
        };
        this.state.transfers.push(transfer);
        this.setState({});
        KubernetesFrontendService.INSTANCE.transfert(transfer)
        .then(r => {
            transfer.result = r;
            this.setState({});
        })
        .catch(err => {
            transfer.result = {
                success: false,
                error: ErrorsService.INSTANCE.jsonify(err),
            }
            this.setState({});
        });
    }

    private clearTransfer()
    {
        this.setState({
            transfers: this.state.transfers.filter(item => {
                return item.result === null || item.result === undefined;
            }),
        });
    }

    render() {
        const textStyle: CSSProperties = {
            textAlign: 'center',
            padding: '1em',
        };
        return (
            <Renderer.Component.TabLayout>
                <Paper elevation={0}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <FileSelector file={this.from}/>
                        </Grid>
                        <Grid item xs={4}></Grid>
                        <Grid item xs={4} padding="auto">
                            <Box display="flex" justifyContent="center">
                                <ArrowDownward fontSize="large"/>
                            </Box>
                        </Grid>
                        <Grid item xs={12}>
                            <FileSelector file={this.to}/>
                        </Grid>
                        <Grid item xs={4}></Grid>
                        <Grid item xs={4}>
                            <Box display="flex" justifyContent="center">
                                <ButtonGroup size="large" aria-label="large outlined primary button group">
                                    <Button onClick={this.transfert}>{I18nService.translate(IDs.WORDS_TRANSFER)}</Button>
                                </ButtonGroup>
                            </Box>
                        </Grid>
                    </Grid>
                </Paper>
                {this.state.transfers.length === 0 ? '' :
                    <Paper elevation={0} style={{
                        marginTop: '2em',
                    }}>
                        {this.state.transfers.map(item => {
                            return <Grid container spacing={2}>
                                <Grid item xs={4} style={textStyle}>
                                    {item.from.type === 'local' ? item.from.path : `${item.from.namespace}/${item.from.deployment}:${item.from.path}`}
                                </Grid>
                                <Grid item xs={4} style={textStyle}>
                                    {item.to.type === 'local' ? item.to.path : `${item.to.namespace}/${item.to.deployment}:${item.to.path}`}
                                </Grid>
                                <Grid item xs={4} style={textStyle}>
                                    {item.result
                                    ? (
                                        item.result.success
                                        ? I18nService.translate(IDs.WORDS_SUCCESS)
                                        : I18nService.translate(IDs.WORDS_FAILURE)
                                    )
                                    : I18nService.translate(IDs.WORDS_RUNNING)}
                                </Grid>
                                {item.result?.success === false && item.result?.error
                                ? <Grid item xs={12}>
                                    {JSON.stringify(item.result.error)}
                                </Grid>
                                : ''}
                            </Grid>;
                        })}

                        <Box display="flex" justifyContent="center">
                            <ButtonGroup size="large" aria-label="large outlined primary button group">
                                <Button onClick={this.clearTransfer}>{I18nService.translate(IDs.WORDS_CLEAR)}</Button>
                            </ButtonGroup>
                        </Box>
                    </Paper>
                }
            </Renderer.Component.TabLayout>
        )
    }
}