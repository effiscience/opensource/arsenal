import { Parameter } from '../types/parameter';
import { Common } from '@k8slens/extensions';
import { makeObservable, observable, action } from 'mobx';
import { IDs } from '../ids';
import { Configuration } from '../types/configuration';
import { ConfigurationParameters } from '../types/configuration-parameters';
import { ConfigurationEntity } from '../types/configuration-entity';
import { Modules } from '../types/modules';

export interface ArsenalStoreModel {
  configurations: {
    default: Configuration,
    [keys: string]: Configuration,
  },
  parameters: {
    default: ConfigurationParameters,
    [keys: string]: ConfigurationParameters,
  }
  modules: {
    default: Modules,
    [keys: string]: Modules,
  }
}

const ArsenalStoreModelDefaults : ArsenalStoreModel = {
  configurations: {
    default: {
      translatableName: true,
      name: IDs.PLUGIN_ID,
      readOnlyParameters: false,
      parametersId: "default",
      modulesId: "default",
    },
  },
  parameters: {
    default: {
      system: {
        language: {
          type: 'radio',
          translatableDisplayName: true,
          displayName: IDs.WORD_LANGUAGE,
          value: 'en',
          oneOf: [
            'en',
            'fr',
          ]
        },
      },
      user: {}
    }
  },
  modules: {
    default: {
      
    }
  },
}

export class ArsenalStore extends Common.Store.ExtensionStore<{data: string}> {
    static GetParameters(entity: ConfigurationEntity): {[parameter: string]: string}
    {
      const parameters: any = {};

      for (let item of Object.keys(entity.parameters.user)) {
        parameters[item] = entity.parameters.user[item].value;
      }

      for (let item of Object.keys(entity.parameters.system)) {
        parameters[item] = entity.parameters.system[item].value;
      }

      return parameters;
    }

    private model: ArsenalStoreModel = ArsenalStoreModelDefaults;
  
    @observable
    private data: string = JSON.stringify(ArsenalStoreModelDefaults);

    constructor() {
      super({
        configName: IDs.PLUGIN_ID,
        defaults: {
            data: JSON.stringify(ArsenalStoreModelDefaults),
        },
      });
      makeObservable(this);
    }
  
    protected fromStore({data}: {data: string}): void {
      this.data = data;
      this.model = JSON.parse(this.data);
    }
  
    toJSON(): {data: string} {
      return {
        data: this.data,
      };
    }

    getConfiguration(id: string): ConfigurationEntity
    {
        if (!this.model.configurations[id]) {
          throw new Error("Undefined '" + id + "' configuration.");
        }

        let entity = JSON.parse(JSON.stringify(this.model.configurations[id])) as ConfigurationEntity;
        if (!entity.parametersId || !this.model.parameters[entity.parametersId]) {
          throw new Error("Undefined '" + entity.parametersId + "' parameters configuration related to '" + id + "' id.");
        }

        entity.parameters = entity.readOnlyParameters
          ? JSON.parse(JSON.stringify(this.model.parameters[entity.parametersId]))
          : this.model.parameters[entity.parametersId];
        
        entity.modules = this.model.modules[entity.modulesId];

        return entity;
    }

    getDefaultConfiguration(): ConfigurationEntity
    {
      return this.getConfiguration('default');
    }

    @action
    save()
    {
      this.data = JSON.stringify(this.model);
    }
  }