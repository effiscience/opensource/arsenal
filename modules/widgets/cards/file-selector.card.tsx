import { Home } from "@mui/icons-material";
import { Box, Button, ButtonGroup, Card, CardContent, Grid, Input, InputAdornment, MenuItem, Select, TextField, Typography, SelectChangeEvent } from "@mui/material";
import React, { ChangeEvent } from "react";
import {I18nService} from "../../i18n/i18n.service";
import {IDs} from "../../ids";
import {File} from "../../types/file";

import { KubernetesFrontendService } from "../../services/kubernetes/kubernetes.frontend-service";

export class FileSelector extends React.Component<{file: File}, {}> {

    constructor(props: any)
    {
        super(props);
        this.setTypeAsLocal = this.setTypeAsLocal.bind(this);
        this.setTypeAsRemote = this.setTypeAsRemote.bind(this);
        this.setNamespace = this.setNamespace.bind(this);
        this.setDeployment = this.setDeployment.bind(this);
        this.setPath = this.setPath.bind(this);

        KubernetesFrontendService.INSTANCE.readNamespaces()
            .then(namespaces => {
                this.props.file.namespaces = namespaces;
                this.setState({});
            });
    }

    setTypeAsLocal()
    {
        this.props.file.type = 'local';
        this.setState({});
    }

    setTypeAsRemote()
    {
        this.props.file.type = 'remote';
        this.setState({});
    }

    setNamespace(event: SelectChangeEvent)
    {
        const namespace = event.target.value;
        this.props.file.namespace = namespace;
        this.setState({});

        KubernetesFrontendService.INSTANCE.readDeployments(namespace)
            .then(deployments => {
                if (this.props.file.namespace === namespace) {
                    this.props.file.deployments = deployments;
                    this.setState({});
                }
            }) 
    }

    setDeployment(event: ChangeEvent<HTMLInputElement>)
    {
        this.props.file.deployment = event.target.value;
        this.setState({});
    }

    setPath(event: ChangeEvent<HTMLInputElement>)
    {
        this.props.file.path = event.target.value;
        this.setState({});
    }

    render(): React.ReactNode {
        const pathComponent = <TextField
            fullWidth={true}
            id="path"
            label={I18nService.translate(IDs.STRINGS_OS_PATH)}
            defaultValue={this.props.file.path || ''}
            InputProps={{
                startAdornment: (
                    <InputAdornment position="start">
                        <Home/>
                    </InputAdornment>
                ),
            }}
            variant="standard"
            onChange={this.setPath}/>;

        const content = this.props.file.type === 'local'
            ? pathComponent
            : <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Select id="namespace"
                                label={I18nService.translate(IDs.WORDS_NAMESPACES)}
                                fullWidth={true}
                                defaultValue={this.props.file.namespace || ''}
                                onChange={this.setNamespace}>
                            {this.props.file.namespaces.map(item => <MenuItem value={item}>{item}</MenuItem>)}
                        </Select>
                    </Grid>
                    <Grid item xs={6}>
                        <Select id="deployment"
                                label={I18nService.translate(IDs.WORDS_DEPLOYMENTS)}
                                fullWidth={true}
                                defaultValue={this.props.file.deployment || ''}
                                disabled={this.props.file.namespace == null || this.props.file.deployments == null || this.props.file.deployments.length == 0}
                                onChange={this.setDeployment}>
                            {(this.props.file.deployments || []).map(item => <MenuItem value={item}>{item}</MenuItem>)}
                        </Select>
                    </Grid>
                    <Grid item xs={12}>
                        {pathComponent}
                    </Grid>
                </Grid>;

        return <Card sx={{ minWidth: 275 }}>
            <CardContent>
                <Box display="flex" justifyContent="center">
                    <ButtonGroup variant="contained" size="large" aria-label="large outlined primary button group">
                        <Button onClick={this.setTypeAsLocal} disabled={this.props.file.type === 'local'}>{I18nService.translate(IDs.WORDS_LOCAL)}</Button>
                        <Button onClick={this.setTypeAsRemote} disabled={this.props.file.type === 'remote'}>{I18nService.translate(IDs.WORDS_REMOTE)}</Button>
                    </ButtonGroup>
                </Box>
            </CardContent>
            <CardContent>
                <Box display="flex">
                    {content}
                </Box>
            </CardContent>
        </Card>;
    }

}