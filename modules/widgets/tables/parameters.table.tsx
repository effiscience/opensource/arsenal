import { FormControlLabel, Paper, Radio, RadioGroup, Stack, Grid, TextField, Button, Box } from "@mui/material";
import React, { ChangeEvent } from "react";
import { observer } from "mobx-react";
import { Parameter } from "../../types/parameter";
import { Parameters } from "../../types/parameters";
import { ArsenalStore } from "../../stores/arsenal.store";
import { I18nService } from "../../i18n/i18n.service";

@observer
export class ParametersTable extends React.Component<{
    store: ArsenalStore,
    parameters: Parameters,
    editable?: boolean,
    deletable?: boolean,
}, {}> {
    
    constructor(props: any)
    {
        if (props.editable === undefined || props.editable === null) {
            props.editable = true;
        }

        if (props.deletable === undefined || props.deletable === null) {
            props.deletable = true;
        }

        super(props);
    }

    componentWillReceiveProps(props: any)
    {
        this.setState({});
    }

    private setParameterValue(parameter: Parameter, event: ChangeEvent<HTMLInputElement>)
    {
        parameter.value = event.target.value;
        ArsenalStore.getInstance().save();
        this.setState({});
    }

    private deleteParameterValue(key: string, event: ChangeEvent<HTMLInputElement>)
    {
        delete this.props.parameters[key];
        ArsenalStore.getInstance().save();
        this.setState({});
    }

    private buildParameterNode(parameter: Parameter): React.ReactNode
    {
        if (parameter.type === 'radio') {
            return <RadioGroup
                defaultValue={parameter.value}
                name="radio-buttons-group">
                {(parameter.oneOf || []).map(choice => <FormControlLabel value={choice} control={<Radio onChange={this.setParameterValue.bind(this, parameter)} />} label={choice} />)}
            </RadioGroup>;
        }

        return <TextField fullWidth={true} value={parameter.value}/>;
    }

    render(): React.ReactNode 
    {
        const columnSize = this.props.deletable ? 4 : 6;
        return <Paper elevation={0}>
            <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={0}
        >
                {Object.keys(this.props.parameters).map(key => {
                    const parameter = this.props.parameters[key];
                    return <Grid container spacing={2}>
                        <Grid item xs={columnSize} sx={{ 
                            textAlign: 'center',
                            fontWeight: 'bold',
                        }}>
                            {parameter.translatableDisplayName ? I18nService.translate(parameter.displayName) : parameter.displayName}
                        </Grid>
                        <Grid item xs={columnSize}>
                            {this.buildParameterNode(parameter)}
                        </Grid>
                        {this.props.deletable ? <Grid item xs={columnSize} padding="auto">
                            <Box display="flex" justifyContent="center">
                                <Button onClick={this.deleteParameterValue.bind(this, key)}>Delete</Button>
                            </Box>
                        </Grid> : ""}
                    </Grid>;
                })}
            </Stack>
        </Paper>;
    }
}