import React from "react";
import {Renderer} from "@k8slens/extensions";

export class EmptyPage extends React.Component<{ extension: Renderer.LensExtension }> {
    render() {
        return (
            <Renderer.Component.TabLayout/>
        )
    }
}
