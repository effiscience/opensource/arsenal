export enum IDs {
    // ###> plugin ###
    PLUGIN_ID                       = 'arsenal',
    PLUGIN_PAGES_VARIABLES          = 'arsenal.pages.variables',
    PLUGIN_PAGES_FILES              = 'arsenal.pages.files',
    PLUGIN_PAGES_CMD                = 'arsenal.pages.cmd',
    
    PLUGIN_VARIABLES_SYSTEM         = 'arsenal.variables.system',
    PLUGIN_VARIABLES_USER           = 'arsenal.variables.user',

    // ###> words ###
    WORDS_LOCAL                     = 'words.local',
    WORDS_REMOTE                    = 'words.remote',
    WORDS_NAMESPACES                = 'words.namespaces',
    WORDS_DEPLOYMENTS               = 'words.deployments',
    WORDS_SAVE                      = 'words.save',
    WORD_LANGUAGE                   = 'words.language',
    WORDS_TRANSFER                  = 'words.transfer',
    WORDS_SUCCESS                   = 'words.success',
    WORDS_FAILURE                   = 'words.failure',
    WORDS_RUNNING                   = 'words.running',
    WORDS_CLEAR                     = 'words.clear',
    
    // ###> strings ###
    STRINGS_OS_PATH                 = 'strings.os.path',


    // ###> errors ###
    ERRORS_FILE_NOT_FOUND           = 'errors.file_not_found',
    ERRORS_FILE_MOVE_FAILURE        = 'errors.file_move_failure',
}