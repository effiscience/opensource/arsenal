import { Common, Renderer } from "@k8slens/extensions";
import { Handyman } from "@mui/icons-material";
import React from "react";
import { IDs } from "./ids";
import { FilesMenu } from "./menus/files/files.menu";
import { VariablesMenu } from "./menus/variables/variables.menu";
import { ArsenalStore } from "./stores/arsenal.store";

export interface PluginPage {
    id: string,
    icon?: (props: Renderer.Component.IconProps) => any;
    display?: (extension: Renderer.LensExtension, store: ArsenalStore, arg?: any) => any;
    pages?: PluginPage[];
}

export interface Plugin {
    id: string;
    icon: (props: Renderer.Component.IconProps) => any;
    pages: PluginPage[]
}

export const plugin: Plugin = {
    id: IDs.PLUGIN_ID,
    icon(props: Renderer.Component.IconProps) {
        return <Handyman/>
    },
    pages: [{
        id: IDs.PLUGIN_PAGES_VARIABLES,
        display: (ext, store) => <VariablesMenu extension={ext} store={store}/>
    }, {
        id: IDs.PLUGIN_PAGES_FILES,
        display: (ext, store) => <FilesMenu extension={ext} store={store}/>
    }/*, {
        id: IDs.PLUGIN_PAGES_CMD,
    }*/]
};