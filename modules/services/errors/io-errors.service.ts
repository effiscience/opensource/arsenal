import { IDs } from './../../ids';
import { I18nService } from './../../i18n/i18n.service';
import { IoErrorsIDs } from './../../errors/io-errors-ids.enum';
import { JsonError } from './../../types/json-error';
import { Code } from './../../types/code';
import { ErrorsDomains } from './../../errors/errors-domains.enum';
import { ErrorsService } from './errors.service';
import TemplateService from '../../templates/templates.service';
export class IoErrorsService {
    static readonly INSTANCE: IoErrorsService;

    makeCode(id: string): Code
    {
        return ErrorsService.INSTANCE.makeCode(ErrorsDomains.IO, id);
    }

    makeError(id: string, what: string): JsonError
    {
        return {
            code: this.makeCode(id),
            what,
        }
    }

    makeFileNotFoundError(file: string): JsonError
    {
        return {
            code: this.makeCode(IoErrorsIDs.FILE_NOT_FOUND),
            what: TemplateService.translate({
                file,
            }, IDs.ERRORS_FILE_NOT_FOUND),
        }
    }

    makeFileMoveFailureError(from: string, to: string, innerError?: any): JsonError
    {
        return {
            code: this.makeCode(IoErrorsIDs.FILE_NOT_FOUND),
            what: TemplateService.translate({
                from,
                to,
            }, IDs.ERRORS_FILE_MOVE_FAILURE),
            innerError: typeof innerError === 'string' || !innerError
                ? innerError
                : innerError.message,
        }
    }
}