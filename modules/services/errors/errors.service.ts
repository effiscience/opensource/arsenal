import { JsonError } from './../../types/json-error';
import { Code } from './../../types/code';

export class ErrorsService {
    static readonly INSTANCE: ErrorsService;

    private origin: string;

    constructor(origin: string)
    {
        this.origin = origin;
    }

    makeCode(domain: string, id: string): Code
    {
        return {
            origin: this.origin,
            domain,
            id,
        }
    }

    makeError(domain: string, id: string, what: string): JsonError
    {
        return {
            code: this.makeCode(domain, id),
            what,
        }
    }

    jsonify(err: any): JsonError
    {
        return {
            code: this.makeCode('unkown', 'unkown'),
            what: err instanceof Error ? `${err.name}: ${err.message}` : JSON.stringify(err),
        }
    }
}