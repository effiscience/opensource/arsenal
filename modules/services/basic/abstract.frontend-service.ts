export abstract class AbstractFrontendService
{
    private _namespace: string;

    constructor(namespace: string)
    {
        this._namespace = namespace;
    }

    protected send<Type = any, Result = any>(channel: string, data?: Type): Promise<Result>
    {
        return Promise.resolve({} as any as Result);
    }
}