import { ClusterInfo } from "../../types/cluster-info";

export abstract class AbstractBackendService {
    private _namespace: string;
    private _channels: [string, (cluster: ClusterInfo, data: any) => any][];

    constructor(namespace: string)
    {
        this._namespace = namespace;
        this._channels = [];
    }

    protected listen<Type = any, Result = any>(channel: string, handler: (data: ClusterInfo, arg: Type) => Result)
    {
        this._channels.push([channel, handler]);
    }
}