export const KubernetesServiceInfo = {
    namespace: 'kubernetes',
    channels: {
        read_namespaces: 'read_namespaces',
        read_deployments: 'read_deployments',
        transfer: 'transfer',
    }
} 