import { FileTransferResult } from './../../types/file-transfer-result';
import { Common } from '@k8slens/extensions';
import { Renderer } from '@k8slens/extensions';
import { FrontendCachedValue } from './../../utils/frontend-cached-value';
import { FrontendCachedValueByParameter } from './../../utils/frontend-cached-value-by-parameter';
import { CacheIDs } from './../cache.ids';
import { AbstractFrontendService } from '../basic/abstract.frontend-service';
import { KubernetesServiceInfo } from './kubernetes.service-info';
import { FileTransfer } from '../../types/file-transfer';

export class KubernetesFrontendService extends AbstractFrontendService {
    static readonly INSTANCE: KubernetesFrontendService;

    private cachedReadNamespace: FrontendCachedValue<string[]>;
    private cachedReadDeployments: FrontendCachedValueByParameter<string[], string>;

    private constructor()
    {
        super(KubernetesServiceInfo.namespace);

        this.cachedReadNamespace = new FrontendCachedValue(() => {
            return this.send(KubernetesServiceInfo.channels.read_namespaces);
        });

        this.cachedReadDeployments = new FrontendCachedValueByParameter(namespace => {
            return this.send(KubernetesServiceInfo.channels.read_deployments, namespace);
        })
    }

    readNamespaces(useCache: boolean = true): Promise<string[]>
    {
        if (!useCache) {
            this.cachedReadNamespace.purge();
        }

        return this.cachedReadNamespace.getValue();
    }

    readDeployments(namespace: string, useCache: boolean = true): Promise<string[]>
    {
        if (!useCache) {
            this.cachedReadDeployments.purge();
        }

        return this.cachedReadDeployments.getValue(namespace);
    }

    transfert(transfer: FileTransfer): Promise<FileTransferResult>
    {
        return this.send(KubernetesServiceInfo.channels.transfer, transfer);
    }
}