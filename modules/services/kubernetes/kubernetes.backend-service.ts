import { IoErrorsService } from './../errors/io-errors.service';
import { FileTransferResult } from './../../types/file-transfer-result';
import { FileTransfer } from './../../types/file-transfer';
import { ClusterInfo } from './../../types/cluster-info';
import { AbstractBackendService } from "../basic/abstract.backend-service";
import { KubernetesServiceInfo } from "./kubernetes.service-info";
import {File} from "../../types/file";
import * as k8s from "@kubernetes/client-node";
import * as fs from "fs";
import * as path from "path";
import * as crypto from "crypto";
import {WritableStreamBuffer} from "stream-buffers";

export class KubernetesBackendService extends AbstractBackendService {
    static readonly INSTANCE: KubernetesBackendService;

    private constructor()
    {
        super(KubernetesServiceInfo.namespace);
        this.listen(KubernetesServiceInfo.channels.read_namespaces, this.readNamespaces);
        this.listen(KubernetesServiceInfo.channels.read_deployments, this.readDeployments);
        this.listen(KubernetesServiceInfo.channels.transfer, this.transfert);
    }

    readNamespaces(clusterInfo: ClusterInfo): Promise<string[]>
    {
        const kubeconfig = new k8s.KubeConfig();
        kubeconfig.loadFromFile(clusterInfo.kubeconfigPath);
        kubeconfig.setCurrentContext(clusterInfo.kubecontext);

        const k8sApi = kubeconfig.makeApiClient(k8s.CoreV1Api);
        return k8sApi.listNamespace().then(namespaces => {
            return namespaces.body.items.map(item => {
                return item.metadata.name;
            });
        });
    }

    readDeployments(clusterInfo: ClusterInfo, namespace: string): Promise<string[]>
    {
        const kubeconfig = new k8s.KubeConfig();
        kubeconfig.loadFromFile(clusterInfo.kubeconfigPath);
        kubeconfig.setCurrentContext(clusterInfo.kubecontext);

        const k8sApi = kubeconfig.makeApiClient(k8s.AppsV1Api);
        return k8sApi.listNamespacedDeployment(namespace).then(deployments => {
            return deployments.body.items.map(item => {
                return item.metadata.name;
            });
        });
    }

    async uploadFile(clusterInfo: ClusterInfo, filename: string, file: File): Promise<FileTransferResult>
    {
        if (!fs.existsSync(filename)) {
            return {
                success: false,
                error: IoErrorsService.INSTANCE.makeFileNotFoundError(filename),
            }
        }

        if (file.type === 'local') {
            try {
                if (file.path.includes('/')) {
                    const directory = path.dirname(file.path);
                    if (!fs.existsSync(directory)) {
                        fs.mkdirSync(path.dirname(file.path), {
                            recursive: true,
                        });
                    }
                }
                fs.renameSync(filename, file.path);
                return {success: true};
            } catch (error) {
                return {
                    success: false,
                    error: IoErrorsService.INSTANCE.makeFileMoveFailureError(filename, file.path, error),
                }
            }
        } else {
            const kubeconfig = new k8s.KubeConfig();
            kubeconfig.loadFromFile(clusterInfo.kubeconfigPath);
            kubeconfig.setCurrentContext(clusterInfo.kubecontext);

            const coreApi = kubeconfig.makeApiClient(k8s.CoreV1Api);
            const appsApi = kubeconfig.makeApiClient(k8s.AppsV1Api);
            const execClient = new k8s.Exec(kubeconfig);

            const deployment = await appsApi.readNamespacedDeployment(file.deployment, file.namespace);
            const labels = deployment.body.spec?.selector?.matchLabels;
            if (!labels || Object.keys(labels).length == 0) {
                return {success: false};
            }

            const pods = await coreApi.listNamespacedPod(file.namespace).then(pods => {
                return pods.body.items.filter(pod => {
                    if (!pod.metadata.labels) {
                        return false;
                    }
                    
                    for (let label of Object.keys(labels)) {
                        if (pod.metadata.labels[label] !== labels[label]) {
                            return false;
                        }
                    }

                    return true;
                });
            });

            for (let pod of pods) {
                await new Promise<void>((resolve, reject) => {
                    const stdin = fs.createReadStream(filename, {
                        encoding: 'UTF8',
                    });
                    stdin.on('error', reject);
                    stdin.on('open', async () => {
                        try {
                            const errStream = new WritableStreamBuffer();
                            await execClient.exec(pod.metadata.namespace, pod.metadata.name, pod.status.containerStatuses[0].name, ['cp', '/dev/stdin',  file.path], null, errStream, stdin, true);
                            stdin.on('close', resolve);
                        } catch(err) {
                            reject(err);
                        }
                    });
                });
            }

            return {success: true};
        }
    }

    async downloadFile(clusterInfo: ClusterInfo, file: File, filename: string): Promise<FileTransferResult>
    {
        if (file.type === 'local') {
            if (!fs.existsSync(file.path)) {
                return {
                    success: false,
                    error: IoErrorsService.INSTANCE.makeFileNotFoundError(file.path),
                }
            }

            try {
                if (filename.includes('/')) {
                    const directory = path.dirname(filename);
                    if (!fs.existsSync(directory)) {
                        fs.mkdirSync(path.dirname(filename), {
                            recursive: true,
                        });
                    }
                }
                fs.renameSync(file.path, filename);
                return {success: true};
            } catch (error) {
                return {
                    success: false,
                    error: IoErrorsService.INSTANCE.makeFileMoveFailureError(file.path, filename, error),
                }
            }
        } else {
            const kubeconfig = new k8s.KubeConfig();
            kubeconfig.loadFromFile(clusterInfo.kubeconfigPath);
            kubeconfig.setCurrentContext(clusterInfo.kubecontext);

            const coreApi = kubeconfig.makeApiClient(k8s.CoreV1Api);
            const appsApi = kubeconfig.makeApiClient(k8s.AppsV1Api);
            const execClient = new k8s.Exec(kubeconfig);

            const deployment = await appsApi.readNamespacedDeployment(file.deployment, file.namespace);
            const labels = deployment.body.spec?.selector?.matchLabels;
            if (!labels || Object.keys(labels).length == 0) {
                return {success: false};
            }

            const pods = await coreApi.listNamespacedPod(file.namespace).then(pods => {
                return pods.body.items.filter(pod => {
                    if (!pod.metadata.labels) {
                        return false;
                    }

                    for (let label of Object.keys(labels)) {
                        if (pod.metadata.labels[label] !== labels[label]) {
                            return false;
                        }
                    }

                    return true;
                });
            });

            if (pods.length === 0) {
                return {success: false};
            } else {
                const pod = pods[0];
                const stdout = fs.createWriteStream(filename);
                const errStream = new WritableStreamBuffer();
                await new Promise<void>((resolve, reject) => {
                    execClient.exec(pod.metadata.namespace, pod.metadata.name, pod.status.containerStatuses[0].name, ['cat', file.path], stdout,  errStream, null, false, () => {
                        stdout.close();
                        if (errStream.size()) {
                            reject(`Error from fetch '${file.path}' file - details: \n ${errStream.getContentsAsString()}`);
                        } else {
                            resolve();
                        }
                    });
                });
                return {success: true};
            }
        }
    }

    transfert(clusterInfo: ClusterInfo, transfer: FileTransfer): Promise<FileTransferResult>
    {
        if (transfer.from.type === 'remote' && transfer.to.type === 'local') {
            return this.downloadFile(clusterInfo, transfer.from, transfer.to.path);
        } else if (transfer.from.type === 'local' && transfer.to.type === 'remote') {
            return this.uploadFile(clusterInfo, transfer.from.path, transfer.to);
        } else {
            const tmp = `/tmp/lens-arsenal-${crypto.randomBytes(4).readUInt32LE(0)}`;
            return this.downloadFile(clusterInfo, transfer.from, tmp)
                .then(result => {
                    if (!result.success) {
                        return result;
                    }

                    return this.uploadFile(clusterInfo, tmp, transfer.to);
                })
                .then(result => {
                    if (fs.existsSync(tmp)) {
                        fs.unlinkSync(tmp);
                    }
                    return result;
                })
        }
    }
}