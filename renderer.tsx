import { Renderer } from "@k8slens/extensions";
import React from "react"

import {plugin, PluginPage} from "./modules/plugin";
import {I18nService, Lang} from "./modules/i18n/i18n.service";
import {Arrays} from "./modules/utils/arrays";

import {EmptyPage} from "./modules/widgets/placeholders/EmptyPage";
import { ArsenalExtensionIpc } from "./renderer.ipc";

import { ArsenalStore } from "./modules/stores/arsenal.store";
import Errors from "./errors";

export default class ArsenalExtension extends Renderer.LensExtension {
    ipc: ArsenalExtensionIpc;
    store: ArsenalStore;
    errors: Errors;

    onActivate() {
        this.errors = new Errors('frontend');
        
        this.store = ArsenalStore.createInstance();
        this.store.loadExtension(this);
        I18nService.setDefaultLang(this.store.getDefaultConfiguration().parameters.system.language.value as Lang);

        this.ipc = ArsenalExtensionIpc.createInstance(this);
        this.ipc.onActivate();

        this.clusterPageMenus.push({
            id: plugin.id,
            title: I18nService.translate(plugin.id),
            components: {
                Icon: plugin.icon,
            },
        });

        const stack: [string, PluginPage[]][] = [[plugin.id, plugin.pages]];
        while (!Arrays.isEmpty(stack)) {
            const [parentId, pages] = Arrays.pop_back(stack);
            for (const page of pages) {
                this.clusterPageMenus.push({
                    parentId,
                    id: page.id,
                    target: {pageId: page.id,},
                    title: I18nService.translate(page.id),
                    components: {
                        Icon: page.icon || null,
                    }
                });

                if (!Arrays.isEmpty(page.pages)) {
                    Arrays.push_back(stack, [page.id, page.pages]);
                }
            }
        }

        Arrays.push_back(stack, [plugin.id, plugin.pages]);
        while (!Arrays.isEmpty(stack)) {
            const [_, pages] = Arrays.pop_back(stack);
            for (const page of pages) {
                this.clusterPages.push({
                    id: page.id,
                    components: {
                        Page: page.display
                            ? page.display.bind(null, this, this.store)
                            : () => <EmptyPage extension={this}/>,
                    }
                });

                if (!Arrays.isEmpty(page.pages)) {
                    Arrays.push_back(stack, [page.id, page.pages]);
                }
            }
        }
    }

    onDeactivate() {
        this.ipc.onDeactivate();
        ArsenalStore.resetInstance();
    }
}