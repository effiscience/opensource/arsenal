import { KubernetesBackendService } from './modules/services/kubernetes/kubernetes.backend-service';
import { Main } from "@k8slens/extensions";
import {AbstractBackendService} from "./modules/services/basic/abstract.backend-service";

export default class ArsenalExtensionMainIpc extends Main.Ipc {
    constructor(extension: Main.LensExtension)
    {
        super(extension);

        const services: any[] = [
            KubernetesBackendService,
        ];

        for (const klass of services) {
            const service = new klass;
            klass.INSTANCE = service;
            const namespace = service._namespace;
            for (const [channel, handler] of service._channels) {
                this.handle(`arsenal::${namespace}::${channel}`, (event, ...args) => {
                    return handler.bind(service)(...args);
                });
            }
        }
    }

    onActivate() {
    }

    onDeactivate() {
        
    }
}