import { Main } from "@k8slens/extensions";
import Errors from "./errors";
import ArsenalExtensionMainIpc from "./main.ipc";
import { ArsenalStore } from "./modules/stores/arsenal.store";

export default class ArsenalExtensionMain extends Main.LensExtension {
    store: ArsenalStore;
    ipc: ArsenalExtensionMainIpc;
    errors: Errors;

    onActivate() {
        this.errors = new Errors('backend');

        this.store = ArsenalStore.createInstance();
        this.store.loadExtension(this);
        
        this.ipc = ArsenalExtensionMainIpc.createInstance(this);
        this.ipc.onActivate();
    }

    onDeactivate() {
        this.ipc.onDeactivate();
    }
}