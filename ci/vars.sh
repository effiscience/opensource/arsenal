#!/bin/bash

set -ea

SHELL_DIRECTORY=$(dirname "${BASH_SOURCE[0]}")
SRC_DIRECTORY="$SHELL_DIRECTORY/.."
DIST_DIRECTORY="$SRC_DIRECTORY/dist"
NODE_MODULES_DIRECTORY="$SRC_DIRECTORY/node_modules"

PACKAGES_FOLDERS=(
    "$DIST_DIRECTORY"
    "$NODE_MODULES_DIRECTORY"
)

PACKAGES_FILES=(
    "$SRC_DIRECTORY/README.md"
    "$SRC_DIRECTORY/CHANGELOG.md"
    "$SRC_DIRECTORY/package.json"
    "$SRC_DIRECTORY/package-lock.json"
)

if [ -z "$PACKAGE_DIRECTORY" ]; then
    PACKAGE_DIRECTORY="$SRC_DIRECTORY/pack"
fi

if [ -z "$VERSION" ]; then
    if [ -n "$CI_COMMIT_TAG" ]; then
        VERSION="$CI_COMMIT_TAG"
    else
        VERSION="1.0.0"
    fi
fi

CP_VERBOSE="-v"

if [ "${SILENT,,}" == "true" ]; then
    CP_VERBOSE=""
fi

set +a
