const fs = require('fs');

function createUpdateJsonFileFunction(callback) {
    return function(filename) {
        if (!fs.existsSync(filename)) {
            throw new Error(`${filename} doesn't exist`);
        }

        const contentStr = fs.readFileSync(filename, 'utf-8');
        const content = JSON.parse(contentStr);
        const result = callback(content) || content;
        const resultStr = JSON.stringify(result, null, 4);

        fs.writeFileSync(filename, resultStr);
    }
}

const updatePackage = createUpdateJsonFileFunction(json => {
    if (process.env.VERSION) {
        json.version = process.env.VERSION;
    }
});

const updatePackageLock = createUpdateJsonFileFunction(json => {
    if (process.env.VERSION) {
        json.version = process.env.VERSION;
    }
});

console.log('--> Update package files');
for (let i = 2; i < process.argv.length; i++) {
    const arg = process.argv[i];
    if (arg.endsWith('package.json')) {
        updatePackage(arg);
    } else if (arg.endsWith('package-lock.json')) {
        updatePackageLock(arg);
    } else {
        throw new Error(`${arg} is not a package file.`);
    }
}