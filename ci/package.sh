#!/bin/bash

set -e

SHELL_DIRECTORY=$(dirname "${BASH_SOURCE[0]}")
source "$SHELL_DIRECTORY/vars.sh"

if [ ! -d "$NODE_MODULES_DIRECTORY" ]; then
    echo "--> Install node modules"
    (cd "$SRC_DIRECTORY" && npm install)
fi

if [ ! -d "$DIST_DIRECTORY" ]; then
    echo "--> Build project"
    (cd "$SRC_DIRECTORY" && npm run build)
fi

echo "--> Create '$PACKAGE_DIRECTORY' package directory"
rm -rf "$PACKAGE_DIRECTORY"
mkdir -p "$PACKAGE_DIRECTORY"

echo "--> Copy directories into package directory"
for folder in "${PACKAGES_FOLDERS[@]}"; do
    cp -r $CP_VERBOSE "$folder" "$PACKAGE_DIRECTORY"
done;

echo "--> Copy files into package directory"
for file in "${PACKAGES_FILES[@]}"; do
    TO_FILE="${file/$SRC_DIRECTORY/$PACKAGE_DIRECTORY}"
    mkdir -p "$(dirname $TO_FILE)"
    cp -f $CP_VERBOSE "$file" "$TO_FILE"
done;

node "$SHELL_DIRECTORY/package.json.js" "$PACKAGE_DIRECTORY/package.json" "$PACKAGE_DIRECTORY/package-lock.json"
(cd $PACKAGE_DIRECTORY && npm pack)

if [ -n "$1" ]; then
    echo "--> Move artefact to $1"
    mkdir -p "$(dirname "$1")"
    mv "$PACKAGE_DIRECTORY/arsenal-$VERSION.tgz" "$1"
fi
