import { Common, Renderer } from "@k8slens/extensions";
import { AbstractFrontendService } from "./modules/services/basic/abstract.frontend-service";
import { KubernetesFrontendService } from "./modules/services/kubernetes/kubernetes.frontend-service";
import { ClusterInfo } from "./modules/types/cluster-info";

export class ArsenalExtensionIpc extends Renderer.Ipc {
    constructor(extension: Renderer.LensExtension) {
        super(extension);
        
        const services: any[] = [
            KubernetesFrontendService,
        ];

        for (const klass of services as any[]) {
            const service = new klass();
            klass.INSTANCE = service;
            const namespace = service._namespace;
            service.send = (channel: string, data: any) => {
                const c = Renderer.Catalog.catalogEntities.activeEntity as Common.Catalog.KubernetesCluster;
                return this.invoke(`arsenal::${namespace}::${channel}`, {
                    id: c.getId(),
                    name: c.getName(),
                    kubecontext: c.spec.kubeconfigContext,
                    kubeconfigPath: c.spec.kubeconfigPath,
                } as ClusterInfo, data);
            }
        }
    }

    onActivate() {
    }

    onDeactivate() {
    }
}