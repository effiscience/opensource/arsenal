import { ErrorsService } from './modules/services/errors/errors.service';

export default class Errors {
    constructor(origin: string)
    {
        function init<Type = any, Klass = new () => Type>(klass: Klass, instance: Type) {
            (klass as any).INSTANCE = instance;
        }

        init(ErrorsService, new ErrorsService(origin));
    }
}