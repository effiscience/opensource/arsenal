# 2022.1.0 changelog

## Features

- Add internationalization feature
- Add template variable
- Add file transfer

## Improvements

## Fixes
